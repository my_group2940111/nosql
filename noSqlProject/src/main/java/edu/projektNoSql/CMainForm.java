package edu.projektNoSql;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;

public class CMainForm extends JFrame{

    private JPanel MainPanel;
    private JRadioButton booksRadioButton;
    private JButton showButton;
    private JTextField bookIdTextField;
    private JList list1;
    private JCheckBox titleCheckBox;
    private JComboBox genreComboBox;
    private JButton raportButton;
    private JButton searchButton;
    private JButton borrowReturnButton;
    private JTextField searchTextField;
    private JCheckBox authorCheckBox;
    private JCheckBox seriesCheckBox;
    private JCheckBox publishingHouseCheckBox;
    private JCheckBox codeISBNCheckBox;
    private JCheckBox genreCheckBox;
    private JButton logoutButton;
    private JRadioButton authorRadioButton;
    private JRadioButton historyRadioButton;
    private JRadioButton borrowsRadioButton;
    private JRadioButton borrowRadioButton;
    private JRadioButton returnRadioButton;
    private JButton clearButton;
    private JTextArea textArea1;

    public CMainForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);

        showButton.addActionListener(e -> this.button1Click());
        borrowReturnButton.addActionListener(e -> {
            try {
                this.button2Click();
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        });
        searchButton.addActionListener(e -> this.button3Click());
        logoutButton.addActionListener(e -> this.button4Click());
        raportButton.addActionListener(e -> this.button5Click());
        clearButton.addActionListener(e -> this.button6Click());
    }


    private void button1Click(){
        if (this.booksRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllBooks());
        }
        if (this.authorRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllAuthors());
        }
        if (this.borrowsRadioButton.isSelected()){
            textArea1.append(DBHelper.printUserBorrows());
        }
        if (this.historyRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllUserBorrows());
        }
    }

    private void button2Click() throws ParseException {
        String bookId = bookIdTextField.getText();
        if(bookId.isEmpty()){
            JOptionPane.showMessageDialog(this,"Nie podano ID książki.\n");
            return;
        }
        if(!DBHelper.checkBookExists(bookId)){
            JOptionPane.showMessageDialog(this,"Książka o podanym ID nie istnieje.\n");
            return;
        }

            if (this.borrowRadioButton.isSelected()){
                boolean borrowed = DBHelper.checkIfBookIsBorrowed(bookId);
                String borrowedByUser = DBHelper.getBorrowId(bookId, GlobalVariables.userName);
                if(borrowedByUser != null){
                    JOptionPane.showMessageDialog(this, "Książka o ID: " + bookId + " jest już wypożyczona.\n");
                    return;
                } else if (borrowed) {
                    JOptionPane.showMessageDialog(this, "Książka o ID: " + bookId + " jest wypożyczona przez innego użytkownika.\n");
                    return;
                }else{
                    textArea1.append(DBHelper.borrowBook(bookId));
                }

            }
            if (this.returnRadioButton.isSelected()){
                boolean borrowed = DBHelper.checkIfBookIsBorrowed(bookId);
                String borrowedByUser = DBHelper.getBorrowId(bookId, GlobalVariables.userName);
                if(borrowedByUser != null){
                    textArea1.append(DBHelper.returnBook(bookId));
                }  else if (borrowed) {
                    JOptionPane.showMessageDialog(this, "Książka o ID: " + bookId + " jest wypożyczona przez innego użytkownika.\n");
                }else {
                    JOptionPane.showMessageDialog(this, "Książka o ID: " + bookId + " nie jest wypożyczona.\n");

                }
            }

    }


    private void button3Click(){
        String title = "";
        String publishingHouse = "";
        String author = "";
        String codeIsbn = "";
        String series = "";
        String genre = "";
        if(titleCheckBox.isSelected()){
            title = searchTextField.getText();
        }
        if(publishingHouseCheckBox.isSelected()){
            publishingHouse = searchTextField.getText();
        }
        if(authorCheckBox.isSelected()){
            author = searchTextField.getText();
        }
        if(codeISBNCheckBox.isSelected()){
            codeIsbn = searchTextField.getText();
        }
        if(seriesCheckBox.isSelected()){
            series = searchTextField.getText();
        }
        if(genreCheckBox.isSelected()){
            int selectedIndex = genreComboBox.getSelectedIndex();
            genre = genreComboBox.getModel().getElementAt(selectedIndex).toString();

        }

        textArea1.append(DBHelper.searchBooks(title, publishingHouse, author, codeIsbn, series, genre));

    }

    private void button4Click(){
        CStartForm form = new CStartForm("Biblioteka");
        GlobalVariables.userName = "";
        form.setVisible(true);
        this.setVisible(false);
    }

    private void button5Click(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(1);
        fileChooser.setDialogTitle("Nazwa pliku raportu");
        fileChooser.setFileFilter(new FileNameExtensionFilter("Plik tekstowy", "txt"));
        int answer = fileChooser.showSaveDialog(null);
        if (answer == 0) {
            String fname = fileChooser.getSelectedFile().toString();
            if (!fname.endsWith(".txt")) {
                fname = fname + ".txt";
            }

            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(fname));

                try {
                    this.textArea1.write(bw);
                    JOptionPane.showMessageDialog(this, "Zapisano plik raportu:\n" + fname);
                } catch (Throwable var8) {
                    try {
                        bw.close();
                    } catch (Throwable var7) {
                        var8.addSuppressed(var7);
                    }
                    throw var8;
                }
                bw.close();
            } catch (IOException var9) {
                JOptionPane.showMessageDialog(this, "Nieudany zapis pliku raportu:\n" + fname);
            }
        }
    }

    private void button6Click(){
        textArea1.setText("");
    }

}