package edu.projektNoSql;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(
                ()-> new CStartForm("Biblioteka").setVisible(true)
        );

    }
}