package edu.projektNoSql;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class GenerateReaders {

    private static final Random RANDOM = new Random();
    private static final String[] NAME = {"Adam", "Michał", "Łukasz", "Czesław", "Cezary", "Dariusz", "Kamil", "Grzegorz", "Mateusz", "Jan"};
    private static final String[] SURNAME = {"Nowak", "Kowalski", "Wiśniewski", "Dąbrowski", "Lewandowski", "Wójcik", "Kamiński", "Kowalczyk", "Zieliński", "Szymański"};
    private static final String[] STREET = {"Krakowska", "Warszawska", "Poznańska", "Łódzka", "Słowackiego", "Jana Pawła II", "Kościuszki", "Józefa Piłsudskiego", "3 Maja", "Armii Krajowej"};
    private static final String[] CITY = {"Warszawa", "Kraków", "Łódź", "Poznań", "Gdańsk", "Szczecin", "Bydgoszcz", "Lublin", "Katowice", "Białystok"};
    private static final String[] PROVINCE = {"mazowieckie", "małopolskie", "łódzkie", "wielkopolskie", "pomorskie", "kujawsko-pomorskie", "dolnośląskie", "lubelskie", "śląskie", "podlaskie"};


    public static boolean generateReaders(int numberOfReaders) {
        try {
            MongoDatabase database = MongoDbConnect.getDatabase();
            MongoCollection<Document> usersCollection = database.getCollection("readers");

            for (int i = 0; i < numberOfReaders; i++) {
                String name = NAME[RANDOM.nextInt(NAME.length)];
                String surname = SURNAME[RANDOM.nextInt(SURNAME.length)];
                String street = STREET[RANDOM.nextInt(STREET.length)];
                String city = CITY[RANDOM.nextInt(CITY.length)];
                String province = PROVINCE[RANDOM.nextInt(PROVINCE.length)];
                String country = "Polska";
                String username = name + surname;
                String password = name + surname + "123*";
                String secondNameString;
                do {
                    secondNameString = NAME[RANDOM.nextInt(NAME.length)];
                } while (secondNameString.equals(name));
                String peselNumber = String.format("%d%02d%02d%05d", RANDOM.nextInt(50) + 50, RANDOM.nextInt(12) + 1, RANDOM.nextInt(28) + 1, RANDOM.nextInt(100000));
                String email = username + "@gmail.com";
                String phoneNumber = String.format("%02d %03d %04d", RANDOM.nextInt(100), RANDOM.nextInt(1000), RANDOM.nextInt(10000));
                LocalDate date = LocalDate.of(RANDOM.nextInt(73) + 1950, RANDOM.nextInt(12) + 1, RANDOM.nextInt(28) + 1);
                Date dateOfBirth = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                String houseNumber = Integer.toString(RANDOM.nextInt(100) + 1);
                String apartmentNumber = Integer.toString(RANDOM.nextInt(100) + 1);
                List<String> books = new ArrayList<>();

                String id = DBHelper.getLastIdFromReaders();

                Document user = new Document("_id", id);
                user.append("userName", username);
                user.append("password", password);
                user.append("personalInfo",
                        new Document("name", name)
                                .append("secondName", secondNameString)
                                .append("lastName", surname)
                                .append("dateOfBirth", dateOfBirth)
                                .append("peselNumber", peselNumber));
                user.append("address",
                        new Document("street", street)
                                .append("houseNumber", Integer.parseInt(houseNumber))
                                .append("apartmentNumber", Integer.parseInt(apartmentNumber))
                                .append("city", city)
                                .append("province", province)
                                .append("country", country));
                user.append("contacts",
                        new Document("email", email)
                                .append("phoneNumber", Integer.parseInt(phoneNumber.replace(" ", ""))));
                user.append("borrows", books);
                usersCollection.insertOne(user);
            }
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
