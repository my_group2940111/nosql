package edu.projektNoSql;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;

public class CRegisterForm extends JFrame {
    private JPanel MainPanel;
    private JTextField nameTextField;
    private JComboBox birthYearComboBox;
    private JCheckBox consent1CheckBox;
    private JButton createAccountButton;
    private JComboBox birthMonthComboBox;
    private JComboBox birthDayComboBox;
    private JTextField secondNameTextField;
    private JTextField lastNameTextField;
    private JTextField peselNumberTextField;
    private JTextField emailTextField;
    private JTextField phoneNumberTextField;
    private JPanel JPanel;
    private JTextField houseNumberTextField;
    private JTextField apartmentNumberTextField;
    private JTextField cityTextField;
    private JTextField postalCodeTextField;
    private JTextField provinceTextField;
    private JComboBox countryTextField;
    private JTextField userNameTextField;
    private JTextField passwordTextField;
    private JTextField passwordCheckTextField;
    private JCheckBox consent2CheckBox;
    private JTextField streetTextField;
    private JButton backActionButton;

    public CRegisterForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);


        createAccountButton.addActionListener(e -> {
            try {
                createAccountButtonClick();
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        });

        backActionButton.addActionListener(e -> goBack());
    }
    private void createAccountButtonClick() throws ParseException {
        String name = nameTextField.getText();
        String secondName = secondNameTextField.getText();
        String lastName = lastNameTextField.getText();
        String peselNumber = peselNumberTextField.getText();
        String email = emailTextField.getText();
        String phoneNumber = phoneNumberTextField.getText();
        int selectedIndex = birthYearComboBox.getSelectedIndex();
        String birthYear = birthYearComboBox.getModel().getElementAt(selectedIndex).toString();
        int selectedIndex2 = birthMonthComboBox.getSelectedIndex();
        String birthMonth= birthMonthComboBox.getModel().getElementAt(selectedIndex2).toString();
        int selectedIndex3 = birthDayComboBox.getSelectedIndex();
        String birthDay = birthDayComboBox.getModel().getElementAt(selectedIndex3).toString();
        String street = streetTextField.getText();
        String house = houseNumberTextField.getText();
        String apartamentNumber = apartmentNumberTextField.getText();
        String city = cityTextField.getText();
        String postalCode = postalCodeTextField.getText();
        String province = provinceTextField.getText();
        int selectedIndex4 = countryTextField.getSelectedIndex();
        String country = countryTextField.getModel().getElementAt(selectedIndex4).toString();
        String username = userNameTextField.getText();
        String password = passwordTextField.getText();
        String repassword = passwordCheckTextField.getText();


        if(name.isEmpty() || lastName.isEmpty() || peselNumber.isEmpty() || email.isEmpty() || phoneNumber.isEmpty()
            || street.isEmpty() || house.isEmpty() || city.isEmpty() || postalCode.isEmpty() || province.isEmpty() || username.isEmpty() || password.isEmpty()
            || repassword.isEmpty()){
            JOptionPane.showMessageDialog(this, "Aby utworzyć konto należy podać wszystkie dane.");
            return;
        }

        /*SPRAWDZANIE WSZYTSKICH NA RAZ - JEDEN TEN SAM KOMUNIKAT
        if(!name.matches("[a-zA-Z]+") || !name.matches("[a-zA-Z]+")|| !lastName.matches("[a-zA-Z]+") || (!secondName.isEmpty()&&!secondName.matches("[a-zA-Z]+")) ||  !peselNumber.matches("\\d+") || peselNumber.length() != 9 ||
                !phoneNumber.matches("\\d+") || phoneNumber.length() != 9 || !city.matches("[a-zA-Z]+") || !province.matches("[a-zA-Z]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane dane.");
            return;
        }*/

        //SPRAWDZANIE PO KOLEJI - KOMUNIKATY DOPASOWANE DO NIEPOPRAWNEJ WARTOSCI
        if(!name.matches("[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane imię.");
            return;
        }

        if(!lastName.matches("[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane nazwisko.");
            return;
        }
        if(!secondName.isEmpty()&&!secondName.matches("[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane drugie imię.");
            return;
        }

        if(!peselNumber.matches("\\d+") || peselNumber.length() != 11){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podany pesel. Powinien składać się z 11 cyfr.");
            return;
        }
        if (!email.contains("@")) {
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podany e-mail, nie zawiera '@'.");
            return;
        }

        if(!phoneNumber.matches("\\d+") || phoneNumber.length() != 9){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podany numer telefonu. Powinien składać się z 9 cyfr.");
            return;
        }

        if(!city.matches("[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane miasto.");
            return;
        }

        if(!province.matches("[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+")){
            JOptionPane.showMessageDialog(this, "Nieprawidłowo podane województwo.");
            return;
        }

        if(secondName.isEmpty()){
            secondName = null;
        }
        if(apartamentNumber.isEmpty()){
            apartamentNumber = null;
        }

        if(!password.equals(repassword)){
            JOptionPane.showMessageDialog(this, "Hasło nie jest takie samo w obu polach.");
            return;
        }

        if(!consent1CheckBox.isSelected() || !consent2CheckBox.isSelected()){
            JOptionPane.showMessageDialog(this, "Aby utworzyć konto należy wyrazić zgodę na przetwarzanie danych osobowych i zaakceptować regulamin.");
            return;
        }

        boolean readerExists = DBHelper.checkIfReaderExists(username);
        if(readerExists){
            JOptionPane.showMessageDialog(this, "Użytkownik " + username +" już istnieje. Podaj inną nazwę użytkownika.");
            return;
        }

        String id = DBHelper.getLastIdFromReaders();
        boolean result = DBHelper.insertReader(id, username, password, name, secondName, lastName, (birthYear+"-"+birthMonth+"-"+birthDay),peselNumber, street, house, apartamentNumber, city, province, country, email, phoneNumber);

        if(result){
            JOptionPane.showMessageDialog(this, "Użytkownik " + username +" został zarejestrowany.");
            CStartForm form = new CStartForm("Biblioteka");
            form.setVisible(true);
            this.setVisible(false);
        }

    }

    private void goBack(){
        CStartForm form = new CStartForm("Biblioteka");
        form.setVisible(true);
        this.setVisible(false);
    }
}
