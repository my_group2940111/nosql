package edu.projektNoSql;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class DBHelperTest {

    @Test
    public void AinsertReader() throws ParseException{
        String id = DBHelper.getLastIdFromReaders();
        String userName = "testuser";
        String password = "testpassword";
        String name = "Test";
        String secondName = "User";
        String lastName = "TestUser";
        String dateOfBirth = "2000-01-01";
        String peselNumber = "12345678901";
        String street = "Test Street";
        String houseNumber = "1";
        String apartmentNumber = "1";
        String city = "Test City";
        String province = "Test Province";
        String country = "Test Country";
        String email = "testuser@test.com";
        String phoneNumber = "123456789";

        boolean result = DBHelper.insertReader(id, userName, password, name, secondName, lastName, dateOfBirth, peselNumber, street, houseNumber, apartmentNumber, city, province, country, email, phoneNumber);
        assertTrue(result);

    }


    @Test
    public void BcheckIfReaderExists() {
        boolean result = DBHelper.checkIfReaderExists("testuser");
        assertTrue(result);

    }
    @Test
    public void checkIfReaderPasswordIsCorrect() {
        boolean result = DBHelper.checkIfReaderPasswordIsCorrect("testuser", "testpassword");
        assertTrue(result);

    }

    @Test
    public void checkIfUserIsAdmin() {
        boolean result = DBHelper.checkIfUserIsAdmin("testuser", "testpassword");
        assertFalse(result);
    }



    @Test
    public void printAllBooks() {
        String result = DBHelper.printAllBooks();

        assertTrue(result.length() > 0);

        assertTrue(result.contains("ID:"));
        assertTrue(result.contains("Tytuł:"));
        assertTrue(result.contains("Wydawnictwo:"));
        assertTrue(result.contains("Data wydania:"));
        assertTrue(result.contains("ISBN:"));
        assertTrue(result.contains("Ilość stron:"));
        assertTrue(result.contains("Gatunek:") || result.contains("Gatunki:"));
        assertTrue(result.contains("Autor:") || result.contains("Autorzy:"));

    }

    @Test
    public void searchBooks() {
        String title = "Balladyna";
        String publishingHouse = "SBM";
        String author = "Juliusz Słowacki";
        String codeIsbn = "978-83-8059-732-7";
        String series = "";
        String genre = "dramat";

        String result = DBHelper.searchBooks(title, publishingHouse, author, codeIsbn, series, genre);

        assertTrue(result.contains("Balladyna"));
        assertTrue(result.contains("SBM"));
        assertTrue(result.contains("Juliusz Słowacki"));
        assertTrue(result.contains("978-83-8059-732-7"));
        assertTrue(result.contains("dramat"));
    }

    @Test
    public void printAllReaders() {
        String result = DBHelper.printAllReaders();

        assertTrue(result.length() > 0);

        assertTrue(result.contains("ID:"));
        assertTrue(result.contains("Nazwa użytkownika:"));
        assertTrue(result.contains("Imię:"));
        assertTrue(result.contains("Drugie imię:"));
        assertTrue(result.contains("Nazwisko:"));
        assertTrue(result.contains("Data urodzenia:"));
        assertTrue(result.contains("Adres:"));
        assertTrue(result.contains("Numer telefonu:"));
        assertTrue(result.contains("E-mail:"));
        assertTrue(result.contains("ID wypożycznia uzytkownika:") || result.contains("ID wypożyczeń użytkownika:"));

    }

    @Test
    public void printAllAuthors() {
        String result = DBHelper.printAllAuthors();

        assertTrue(result.length() > 0);
        assertTrue(result.contains("Imię i nazwisko autora:"));
        assertTrue(result.contains("Data urodzenia:"));
        assertTrue(result.contains("Książka") || result.contains("Książki"));

    }

    @Test
    public void printAllBorrows() {
        String result = DBHelper.printAllBorrows();

        assertTrue(result.length() > 0);
        assertTrue(result.contains("Data wypożyczenia książki:"));
        assertTrue(result.contains("Data oddania książki:"));
        assertTrue(result.contains("Wypożyczona książka:"));
        assertTrue(result.contains("ID:"));
        assertTrue(result.contains("Tytuł:"));
        assertTrue(result.contains("Autor:") || result.contains("Autorzy:"));

    }


    @Test
    public void printUserBorrows() {
        String result = DBHelper.printAllBorrows();

        assertTrue(result.length() > 0);
        assertTrue(result.contains("Data wypożyczenia książki:"));
        assertTrue(result.contains("Data oddania książki:"));
        assertTrue(result.contains("Wypożyczona książka:"));
        assertTrue(result.contains("ID:"));
        assertTrue(result.contains("Tytuł:"));
        assertTrue(result.contains("Autor:") || result.contains("Autorzy:"));

    }

    @Test
    public void printAllUserBorrows() {
        String result = DBHelper.printAllBorrows();

        assertTrue(result.length() > 0);
        assertTrue(result.contains("Data wypożyczenia książki:"));
        assertTrue(result.contains("Data oddania książki:"));
        assertTrue(result.contains("Wypożyczona książka:"));
        assertTrue(result.contains("ID:"));
        assertTrue(result.contains("Tytuł:"));
        assertTrue(result.contains("Autor:") || result.contains("Autorzy:"));

    }

    @Test
    public void checkBookExists() {
        boolean result = DBHelper.checkBookExists("1");
        assertTrue(result);
    }

    @Test
    public void getTitleForBorrow() {
        String title = DBHelper.getTitleForBorrow("1");
        assertNotNull(title);
        assertFalse(title.isEmpty());
    }

    @Test
    public void getAuthorsForBorrow() {
        List<String> authors = DBHelper.getAuthorsForBorrow("1");
        assertNotNull(authors);
        assertFalse(authors.isEmpty());
    }

    @Test
    public void getReaderId() {
        String id = DBHelper.getReaderId("testuser");
        assertNotNull(id);
        assertFalse(id.isEmpty());
    }

    @Test
    public void checkIfBookIsBorrowed() {
        boolean result = DBHelper.checkIfBookIsBorrowed("1");
        assertTrue(result);
    }

    @Test
    public void EborrowBook() {
        GlobalVariables.userName = "testuser";
        String result = DBHelper.borrowBook("1");
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }
    @Test
    public void addBookToAuthor() {
        boolean result = DBHelper.addBookToAuthor("Juliusz Słowacki", "Tren X");
        assertTrue(result);
    }

    @Test
    public void addAuthor() {
        String id = DBHelper.getLastIdFromAuthors();
        ArrayList<String> books = new ArrayList<>();
        books.add("Tytuł");
        LocalDate date1 = LocalDate.of(1995, 10, 20);
        java.util.Date dateOfBirth = java.util.Date.from(date1.atStartOfDay(ZoneId.systemDefault()).toInstant());
        boolean result = DBHelper.addAuthor(id, "Anna Kowalska", dateOfBirth, books);
        assertTrue(result);

    }

    @Test
    public void checkIfAuthorExists() {
        boolean result = DBHelper.checkIfAuthorExists("Anna Kowalska");
        assertTrue(result);
    }

    @Test
    public void getLastIdFromBooks() {
        String id = DBHelper.getLastIdFromBooks();
        assertNotNull(id);
        assertFalse(id.isEmpty());
    }

    @Test
    public void getLastIdFromAuthors() {
        String id = DBHelper.getLastIdFromAuthors();
        assertNotNull(id);
        assertFalse(id.isEmpty());
    }

    @Test
    public void getLastIdFromBorrows() {
        String id = DBHelper.getLastIdFromBorrows();
        assertNotNull(id);
        assertFalse(id.isEmpty());
    }

    @Test
    public void getLastIdFromReaders() {
        String id = DBHelper.getLastIdFromReaders();
        assertNotNull(id);
        assertFalse(id.isEmpty());
    }

    @Test
    public void getBorrowId() {
        GlobalVariables.userName = "testuser";
        String idBorrow = DBHelper.getBorrowId("1", GlobalVariables.userName);
        assertNotNull(idBorrow);
        assertFalse(idBorrow.isEmpty());
    }

    @Test
    public void returnBook() {
        GlobalVariables.userName = "testuser";
        String result = DBHelper.returnBook("1");
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }
}