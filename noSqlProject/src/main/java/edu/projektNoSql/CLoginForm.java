package edu.projektNoSql;

import javax.swing.*;
import java.awt.*;

public class CLoginForm extends JFrame{
    private JButton loginButton;
    private JTextField userNameLoginTextField;
    private JTextField passwordLoginTextField;
    private JPanel MainPanel;
    private JButton backActionButton;

    public CLoginForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);
        loginButton.addActionListener(e -> buttonLoginClick());

        backActionButton.addActionListener(e -> goBack());
    }

    private void buttonLoginClick(){

        String username = userNameLoginTextField.getText();
        String password = passwordLoginTextField.getText();

        if(username.isEmpty() || password.isEmpty()){
            JOptionPane.showMessageDialog(this, "Aby się zalogować należy uzupełnić wszystkie pola.");
            return;
        }

       boolean exist =  DBHelper.checkIfReaderExists(username);

       if(!exist){
           JOptionPane.showMessageDialog(this, "Użytkownik " + username +" nie istnieje.");
           return;
       }

       boolean passwordCorrect = DBHelper.checkIfReaderPasswordIsCorrect(username,password);
       boolean userIsAdmin = DBHelper.checkIfUserIsAdmin(username,password);

       if(!passwordCorrect){
           JOptionPane.showMessageDialog(this, "Nieprawidłowe hasło.");
       }else{
           JOptionPane.showMessageDialog(this, "Zalogowano na konto użytkownika " + username + ".");
            GlobalVariables.userName = username;

           if(userIsAdmin){
               CMainAdminForm form = new CMainAdminForm("Biblioteka");
               form.setVisible(true);
               this.setVisible(false);
           }else{
               CMainForm form = new CMainForm("Biblioteka");
               form.setVisible(true);
               this.setVisible(false);
           }
       }
    }

    private void goBack(){
        CStartForm form = new CStartForm("Biblioteka");
        form.setVisible(true);
        this.setVisible(false);
    }
}
