package edu.projektNoSql;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Random;

public class GenerateAuthors {


        private static final Random RANDOM = new Random();
    private static final String[] NAMES = {"Kazuo", "Khaled", "Philip",  "Audrey","Margaret", "Scott","Leo", "Douglas", "Evelyn", "Fyodor", "John", "Kenneth", "Louisa",
            "Daphne", "Sebastian", "Jane", "Charllotte", "Harper", "Emily", "George", "Conan", "Roald", "Victor","Enid", "Joseph", "Antoine", "Iain","Richard", "John",
            "Nevil", "Gustave", "Alice", "Jesse", "Walter", "Helen", "Salman", "Arthur", "Emile",  "Herman", "Bram", "Frances", "Bill", "James", "Joyce", "Sylvia",
            "Arthut", "Emile", "William", "Donna", "Alice", "Alexandre", "Jack", "Thomas", "Mark", "Gabiriel", "John", "Vikram", "Carlos", "Aldous", "Charles", "Emma",
            "Olivia", "Ava", "Isabella", "Sophia", "Mia", "Charlotte", "Amelia", "Harper", "Evelyn", "Abigail", "Emily", "Elizabeth", "Avery", "Sofia", "Ella", "Madison",
            "Scarlett", "Victoria", "Aria", "Grace", "Chloe", "Camila", "Penelope", "Riley", "Layla", "Lillian", "Natalie", "Hazel", "Aubrey", "Adalynn", "Zoey", "Mila",
            "Aaliyah", "Ellie", "Audrey", "Skylar", "Violet", "Claire", "Bella", "Aurora", "Lucy", "Anna", "Savanah", "Aurora", "Everly", "Hannah", "Arianna", "Rylee",
            "Adeline", "Jan", "Anna", "Katarzyna", "Tomasz", "Joanna", "Marcin", "Agnieszka", "Marek", "Magdalena", "Piotr", "Barbara", "Andrzej", "Monika", "Adam",
            "Małgorzata", "Ewa", "Michał", "Danuta", "Jacek", "Zofia"};
    private static final String[] LAST_NAMES = {"Waugh", "De Bernieres", "Dostoyevsky", "Graham", "Hosseini", "Fitzgerald", "Tolstoy", "Niffenegger", "Du Maurier",
            "Austen", "Bronte", "Lee", "Orwell", "Pullman", "Alcott", "Heller", "Shekespeare", "Dahl", "Hugo","Mitchell", "Doyle", "Blyton","Conrad", "De Saint-Exupery",
            "Banks", "Adams", "Kennedy", "Toole", "Shute", "Walker", "Ishiguro", "Flaubert", "White", "Pinkman", "Fielding", "Plath", "Ransome", "Zola", "Makepeace",
            "Byatt", "Rushdie", "Melville", "Dickens", "Stoker","Plath", "Hodgson", "Burnett", "James","Brayson", "Garcia", "Marquez", "Steinbeck", "Tartt", "Sebold",
            "Dumas", "Kerouac", "Hardy", "Seth", "Dickens", "Huxley", "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor",
            "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Young", "Allen", "King", "Wright", "Robinson", "Clark", "Lewis", "Lee", "Walker",
            "Hall", "Allen", "Young", "Hernandez", "Moore", "Wilson", "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Young", "Allen", "King",
            "Wright", "Robinson", "Clark", "Lewis", "Lee", "Walker", "Hall", "Nowak", "Wójcik", "Kowalczyk", "Woźniak", "Mazur",  "Zając","Walczak", "Stępień", "Dudek",
            "Wrona", "Szewczyk", "Stasiak", "Krupa", "Kruk", "Lis", "Bąk", "Urban", "Wróbel", "Guca", "Nitka"};

    public static boolean generateAuthors(int numberOfAuthors) {
        boolean success = false;
            for (int i = 0; i < numberOfAuthors; i++) {
                int nameIndex = RANDOM.nextInt(NAMES.length);
                int lastNameIndex = RANDOM.nextInt(LAST_NAMES.length);
                String name = NAMES[nameIndex];
                String last_name = LAST_NAMES[lastNameIndex];
                String fullName = name + " " + last_name;

                while (DBHelper.checkIfAuthorExists(fullName)) {
                    nameIndex = RANDOM.nextInt(NAMES.length);
                    name = NAMES[nameIndex];
                    last_name = LAST_NAMES[lastNameIndex];
                    fullName = name + " " + last_name;
                }

                LocalDate date1 = LocalDate.of(RANDOM.nextInt(73) + 1920, RANDOM.nextInt(12) + 1, RANDOM.nextInt(28) + 1);
                java.util.Date dateOfBirth = java.util.Date.from(date1.atStartOfDay(ZoneId.systemDefault()).toInstant());
                String id = DBHelper.getLastIdFromAuthors();

                success = DBHelper.addAuthor(id, fullName, dateOfBirth, new ArrayList<>());

            }
            return success;
    }

}

