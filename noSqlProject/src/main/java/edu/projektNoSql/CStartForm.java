package edu.projektNoSql;

import javax.swing.*;
import java.awt.*;

public class CStartForm extends JFrame{
    private JPanel MainPanel;
    private JButton loginButton;
    private JButton registerButton;

    public CStartForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);

        loginButton.addActionListener(e -> loginButtonClick());
        registerButton.addActionListener(e -> registerButtonClick());
    }

    private void registerButtonClick(){
        CRegisterForm form = new CRegisterForm("Biblioteka");
        form.setVisible(true);
        this.setVisible(false);

    }

    private void loginButtonClick(){
        CLoginForm form = new CLoginForm("Biblioteka");
        form.setVisible(true);
        this.setVisible(false);

    }


}
