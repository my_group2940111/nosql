package edu.projektNoSql;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CMainAdminForm extends JFrame{
    private JRadioButton booksRadioButton;
    private JButton showButton;
    private JTextField generateTextField;
    private JCheckBox titleCheckBox;
    private JComboBox genreComboBox;
    private JButton clearButton;
    private JButton searchButton;
    private JButton generateButton;
    private JTextField searchTextField;
    private JCheckBox authorCheckBox;
    private JCheckBox seriesCheckBox;
    private JCheckBox publishingHouseCheckBox;
    private JCheckBox codeISBNCheckBox;
    private JCheckBox genreCheckBox;
    private JButton logoutButton;
    private JRadioButton booksGenerateRadioButton;
    private JRadioButton usersGenerateRadioButton;
    private JPanel MainPanel;
    private JButton raportButton;
    private JTextArea textArea1;
    private JRadioButton authorRadioButton;
    private JRadioButton borrowsRadioButton;
    private JRadioButton usersRadioButton;
    private JRadioButton authorsGenerateRadioButton;

    public CMainAdminForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);

        showButton.addActionListener(e -> this.button1Click());
        generateButton.addActionListener(e -> this.button2Click());
        searchButton.addActionListener(e -> this.button3Click());
        logoutButton.addActionListener(e -> this.button4Click());
        raportButton.addActionListener(e -> this.button5Click());
        clearButton.addActionListener(e -> this.button6Click());

    }


    private void button1Click(){
//        textArea1.append("");
        if (this.booksRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllBooks());
        }
        if (this.authorRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllAuthors());
        }
        if (this.usersRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllReaders());
        }
        if (this.borrowsRadioButton.isSelected()){
            textArea1.append(DBHelper.printAllBorrows());
        }


    }

    private void button2Click(){
        int amount = Integer.parseInt(generateTextField.getText());
        if (this.booksGenerateRadioButton.isSelected()){
            boolean generateInsert = GenerateBooks.generateBooks(amount);
            if(generateInsert){
                textArea1.append("Wygenerowano i dodano " + amount + " dokumentów do kolekcji książek.\n");
            }
            else{
                textArea1.append("Nie udało się wygenerować i dodać do kolekcji książek " + amount + " dokumentów.\n");
            }
        }
        if (this.usersGenerateRadioButton.isSelected()){
            boolean generateInsert = GenerateReaders.generateReaders(amount);
            if(generateInsert){
                textArea1.append("Wygenerowano i dodano " + amount + " dokumentów do kolekcji czytelników.\n");
            }
            else{
                textArea1.append("Nie udało się wygenerować i dodać do kolekcji czytelników " + amount + " dokumentów.\n");
            }
        }
        if(this.authorsGenerateRadioButton.isSelected()){
            boolean generateInsert = GenerateAuthors.generateAuthors(amount);
            if(generateInsert){
                textArea1.append("Wygenerowano i dodano " + amount + " dokumentów do kolekcji autorów.\n");
            }
            else{
                textArea1.append("Nie udało się wygenerować i dodać do kolekcji autorów " + amount + " dokumentów.\n");
            }
        }

    }

    private void button3Click(){

            String title = "";
            String publishingHouse = "";
            String author = "";
            String codeIsbn = "";
            String series = "";
            String genre = "";
            if(titleCheckBox.isSelected()){
                title = searchTextField.getText();
            }
            if(publishingHouseCheckBox.isSelected()){
                publishingHouse = searchTextField.getText();
            }
            if(authorCheckBox.isSelected()){
                author = searchTextField.getText();
            }
            if(codeISBNCheckBox.isSelected()){
                codeIsbn = searchTextField.getText();
            }
            if(seriesCheckBox.isSelected()){
                series = searchTextField.getText();
            }
            if(genreCheckBox.isSelected()){
                int selectedIndex = genreComboBox.getSelectedIndex();
                genre = genreComboBox.getModel().getElementAt(selectedIndex).toString();

            }

            textArea1.append(DBHelper.searchBooks(title, publishingHouse, author, codeIsbn, series, genre));
    }

    private void button4Click(){
        CStartForm form = new CStartForm("Biblioteka");
        GlobalVariables.userName = "";
        form.setVisible(true);
        this.setVisible(false);
    }

    private void button5Click(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(1);
        fileChooser.setDialogTitle("Nazwa pliku raportu");
        fileChooser.setFileFilter(new FileNameExtensionFilter("Plik tekstowy", "txt"));
        int answer = fileChooser.showSaveDialog(null);
        if (answer == 0) {
            String fname = fileChooser.getSelectedFile().toString();
            if (!fname.endsWith(".txt")) {
                fname = fname + ".txt";
            }

            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(fname));

                try {
                    this.textArea1.write(bw);
                    JOptionPane.showMessageDialog(this, "Zapisano plik raportu:\n" + fname);
                } catch (Throwable var8) {
                    try {
                        bw.close();
                    } catch (Throwable var7) {
                        var8.addSuppressed(var7);
                    }
                    throw var8;
                }
                bw.close();
            } catch (IOException var9) {
                JOptionPane.showMessageDialog(this, "Nieudany zapis pliku raportu:\n" + fname);
            }
        }
    }

    private void button6Click(){
        textArea1.setText("");
    }

}
