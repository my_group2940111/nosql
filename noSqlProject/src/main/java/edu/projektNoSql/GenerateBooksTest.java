package edu.projektNoSql;

import org.junit.Test;

import static org.junit.Assert.*;

public class GenerateBooksTest {

    @Test
    public void generateBooks() {

        int numberOfBooks = 10;
        boolean result = GenerateBooks.generateBooks(numberOfBooks);
        assertTrue(result);
    }

    @Test
    public void generateISBN() {
        int numberOfTests = 10;
        for (int i = 0; i < numberOfTests; i++) {
            String isbn = GenerateBooks.generateISBN();
            int length = isbn.length();
            assertTrue(length == 10 || length == 13);
        }
    }
}