package edu.projektNoSql;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import org.bson.Document;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.mongodb.client.model.Filters.eq;

public class GenerateBooks {

    static MongoDatabase database = MongoDbConnect.getDatabase();
    static MongoCollection<Document> authors = database.getCollection("authors");

    static MongoCollection<Document> booksCollection = database.getCollection("books");

    private static final Random RANDOM = new Random();

    private static final String[] TITLES = {"Pan Tadeusz", "W pustyni i w puszczy", "Ogniem i mieczem", "Potop", "Quo Vadis", "Ania z Zielonego Wzgórza",
                "Przedwiośnie", "Dziady", "Złota jesień", "Balladyna", "Dziennikarz", "Wesele", "Lalka", "Kamienie na szaniec", "Krzyżacy", "Opowieści z Narnii",
                "Harry Potter i Kamień Filozoficzny", "Hobbit", "Wichrowe Wzgórza", "Mroczne materie", "Władca Pierścieni", "Lśnienie", "To", "Buszujący w zbożu",
                 "Rok 1984", "Władca Pierścieni", "Hobbit", "Odyseja", "Iliada", "Zbrodnia i kara",  "Ślepnąc od świateł", "Mały Książę", "Żona podróżnika w czasie",
                "Harry Potter i Kamień Filozoficzny",  "Harry Potter i Więzień Azkabanu", "Harry Potter i Czara Ognia",  " Harry Potter i Zakon Feniksa", "Przemineło z wiatrem",
                "Harry Potter i Książe Półkrwi", " Harry Potter i Insygnia Śmierci",  "Boska Komedia", "Dzieci z Bullerbyn",  "Rebeka", "Birdsong","Miasteczko Middlemarch",
                "Alicja w Krainie Czarów", "Kubuś Puchatek", "Duma i uprzedzenie", "Jane Eyre", "Wielkie nadzieje", "Małe kobietki", "Tessa D’Urberville", "Paragraf 22",
                "Wojna i pokój", "Wielki Gatsby", "Pustkowie", "Autostopem przez Galaktykę", "Znowu w Brideshead", "Grona gniewu", "O czym szumią wierzby", "Anna Karenina",
                "David Copperfield", "Perswazje", "Emma", "Lew, Czarownica i Stara Szafa", "Chłopiec z latawcem", "Kapitan Corelli", "Wyznania Gejszy", "Folwark zwierzęcy",
                "Kod Da Vinci", "Sto lat samotności", "Modlitwa za Owena", "Kobieta w bieli", "Z dala od zgiełku", "Opowieści podręcznej", "Władca much", "Życie Pi", "Diuna",
                "Cold Comfort Farm", "Rozważna i romantyczna", "Pretendent do ręki", "Cień wiatru", "Opowieść o dwóch miastach", "Nowy wspaniały świat",
                "Dziwny przypadek psa nocną porą", "Miłość w czasach zarazy", "Myszy i ludzie", "Tajemna historia", "Nostalgia aniłoa", "Hrabia Monte Christo", "W drodze",
                "Juda nieznany", "Dziennik Bridget Jones", "Dzieci północy", "Olivier Twist", "Tajemniczy ogród", "Zapiski z małej wyspy", "Ulisses", "Szklany kosz",
                "Jaskółki i Amazonki", "Germinal", "Targowisko próżności", "Opowieść wigilijna", "Atlas chmur", "Kolor purpury", "Okruchy dnia", "Pani Bovary",
                "A Fine Balance", "Pajęczyna Szarloty", "Pięć osób, które spotykamy w niebie", "Przygody Sherlocka Holmesa", "The Faraway Tree Collection", "Jądro ciemności",
                "Fabryka os", "Wodnikowe Wzgórze", "Sprzysiężenie głupców", "Miasteczko jak Alece Springs", "Trzej muszkieterowie", "Hamlet", "Charlie i fabryka czekolady","Nędznicy"};


        private static final String[] PUBLISHING_HOUSES = {"DRAGON", "MAG", "Amber", "Złote Myśli", "Albatros", "Albatros+", "Słonecznik", "Słowo", "Znak", "Burda Książki",
                "Burda Publishing Polska", "Czarna Owca", "Czwarta Strona", "Drageus", "Egmont", "HarperCollins", "Insignis", "Jaguar", "Jesien", "Latarnik", "Leksykon",
                "Lider", "Literackie", "Mozaika", "Nasza Księgarnia", "Nasze", "Niezwykłe", "Otwarte", "Pascal", "PWN", "Rebis", "Ravi", "Sonic", "Sonia Draga", "Step",
                "Tatrzańska", "Vocatio", "WAM", "Zysk i S-ka", "Znak Emotikon"};
        private static final String[] SERIES = {"Pan Tadeusz", "W pustyni i w puszczy", "Ogniem i mieczem", "Potop", "Quo Vadis", "Ania z Zielonego Wzgórza",
                "Przedwiośnie", "Dziady", "Złota jesień", "Balladyna", "Dziennikarz", "Wesele", "Lalka", "Kamienie na szaniec", "Krzyżacy", "Opowieści z Narnii", "Harry Potter",
                "Hobbit", "Niezgodna", "Gra o tron", "Dawno temu w trawie", "Mroczne materie", "American Gods", "Władca Pierścieni", "Północ w ogrodzie dobra i zła",
                "Lśnienie", "Ciche miejsce", "To", "Noc kryształowa"};

        private static final String[] GENRES = {"Fantasy", "Dla dzieci", "Science fiction", "Thriller", "Romans", "Horror", "Kryminał", "Dramat", "Biografia", "Autobiografia",
                "Podróże", "Reportaż", "Literatura obcojęzyczna", "Literatura polska","Poezja", "Dziennik", "Popularno-naukowe", "Programowanie","Komiks", "Kultura", "Ekonomia",
                "Psychologia", "Edukacyjne", "Nauka"};

    public static boolean generateBooks(int numberOfBooks) {
        try {
            for (int i = 0; i < numberOfBooks; i++) {
                int titleIndex = RANDOM.nextInt(TITLES.length);
                String title = TITLES[titleIndex];
                String publishingHouse = PUBLISHING_HOUSES[RANDOM.nextInt(PUBLISHING_HOUSES.length)];
                LocalDate date = LocalDate.of(RANDOM.nextInt(73) + 1950, RANDOM.nextInt(12) + 1, RANDOM.nextInt(28) + 1);
                java.util.Date releaseDate = java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                String series = null;
                int tomeOfSeries = 0;
                if (RANDOM.nextBoolean()) {
                    int seriesIndex = RANDOM.nextInt(SERIES.length);
                    series = SERIES[seriesIndex];
                    tomeOfSeries = RANDOM.nextInt(20) + 1;
                }
                String codeISBN = generateISBN();
                int numberOfPages = RANDOM.nextInt(900) + 100;
                List<String> genres = new ArrayList<>();
                int numberOfGenres = RANDOM.nextInt(3) + 1;
                for (int j = 0; j < numberOfGenres; j++) {
                    genres.add(GENRES[RANDOM.nextInt(GENRES.length)]);
                }

                List<String> au = new ArrayList<>();
                long count = authors.countDocuments();
                int numberOfAuthors = RANDOM.nextInt(3) + 1;

                for (int j = 0; j < numberOfAuthors; j++) {
                    Document randomAuthor = authors.aggregate(Arrays.asList(
                            Aggregates.sample(1)
                    )).first();

                    String authorName = randomAuthor.getString("name");
                    au.add(authorName);



                    authors.updateOne(
                            eq("_id", randomAuthor.get("_id")),
                            new Document("$addToSet", new Document("books", title))
                    );
                }

                String id = DBHelper.getLastIdFromBooks();
                Document book = new Document("_id", id);
                book.append("title", title);
                book.append("publishingHouse", publishingHouse);
                book.append("releaseDate", releaseDate);
                book.append("series", series);
                book.append("tomeOfSeries", tomeOfSeries);
                book.append("codeISBN", codeISBN);
                book.append("numberOfPages", numberOfPages);
                book.append("genre", genres);
                book.append("authors", au);

                booksCollection.insertOne(book);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }
    public static String generateISBN(){
        Random rand = new Random();
        int length = rand.nextInt(2) + 1;
        StringBuilder isbn = new StringBuilder();
        if (length == 1) {
            for (int i = 0; i < 10; i++) {
                isbn.append(rand.nextInt(10));
            }
        } else {
            for (int i = 0; i < 13; i++) {
                isbn.append(rand.nextInt(10));
            }
        }
        return isbn.toString();
    }
}




