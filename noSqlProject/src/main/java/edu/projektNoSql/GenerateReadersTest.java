package edu.projektNoSql;

import org.junit.Test;

import static org.junit.Assert.*;

public class GenerateReadersTest {

    @Test
    public void generateReaders() {
        int numberOfReaders = 10;
        boolean result = GenerateReaders.generateReaders(numberOfReaders);
        assertTrue(result);
    }
}