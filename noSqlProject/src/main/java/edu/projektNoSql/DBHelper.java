package edu.projektNoSql;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonDateTime;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class DBHelper{

    static MongoDatabase database = MongoDbConnect.getDatabase();
    static MongoCollection<Document>  readers = database.getCollection("readers");
    static MongoCollection<Document> books = database.getCollection("books");
    static MongoCollection<Document> authors = database.getCollection("authors");
    static MongoCollection<Document> borrows = database.getCollection("borrows");
    public static boolean checkIfReaderExists(String name) {

        BasicDBObject query = new BasicDBObject();
        query.put("userName", name);

        MongoCursor<Document> cursor = readers.find(query).iterator();
        return cursor.hasNext();
    }

    public static boolean checkIfReaderPasswordIsCorrect(String login, String password) {

        BasicDBObject query = new BasicDBObject();
        query.put("userName", login);
        query.put("password", password);

        MongoCursor<Document> cursor = readers.find(query).iterator();
        boolean isCorrect = cursor.hasNext();
        cursor.close();
        return isCorrect;
    }

    public static boolean checkIfUserIsAdmin(String login, String password) {

        BasicDBObject query = new BasicDBObject();
        query.put("userName", login);
        query.put("password", password);

        MongoCursor<Document> cursor = readers.find(query).iterator();
        boolean isCorrect = cursor.hasNext();
        cursor.close();

        String adminLogin = "admin";
        String adminPassword = "admin";
        if (login.equals(adminLogin) && password.equals(adminPassword)) {
            return isCorrect;
        } else {
            return false;
        }
    }

    public static boolean insertReader(String id, String userName, String password,
                                       String name, String secondName, String lastName,
                                       String dateOfBirth, String peselNumber,
                                       String street, String houseNumber, String apartmentNumber,
                                       String city, String province, String country,
                                       String email, String phoneNumber) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(dateOfBirth));
        java.sql.Date birthdate = new java.sql.Date(cal.getTimeInMillis());

        Document personalInfo = new Document("name", name)
                .append("secondName", secondName)
                .append("lastName", lastName)
                .append("dateOfBirth", new BsonDateTime(birthdate.getTime()))
                .append("peselNumber", peselNumber);
        Document address = new Document("street", street)
                .append("houseNumber", houseNumber)
                .append("apartmentNumber", apartmentNumber)
                .append("city", city)
                .append("province", province)
                .append("country", country);
        Document contacts = new Document("email", email)
                .append("phoneNumber", phoneNumber);
        Document reader = new Document("_id", id)
                .append("userName", userName)
                .append("password", password)
                .append("personalInfo", personalInfo)
                .append("address", address)
                .append("contacts", contacts);
        InsertOneResult result = readers.insertOne(reader);
        BsonValue _id = result.getInsertedId();
        return _id != null;
    }


    public static String printAllBooks() {

        FindIterable<Document> booksDoc = books.find();
        List<Document> bookList = new ArrayList<>();
        booksDoc.forEach(bookList::add);

        return getFormattedResultBooks(bookList);

    }


    public static String searchBooks(String title, String publishingHouse, String author, String codeIsbn, String series, String genre) {
        Bson filter = new Document();
        List<Bson> filters = new ArrayList<>();

        addFilter("title", title, filters);
        addFilter("publishingHouse", publishingHouse, filters);
        addFilter("authors", author, filters);
        addFilter("codeISBN", codeIsbn, filters);
        addFilter("series", series, filters);
        addFilter("genre", genre, filters);

        if (!filters.isEmpty()) {
            filter = Filters.or(filters);
        }

        FindIterable<Document> booksDoc = books.find(filter);
        List<Document> bookList = new ArrayList<>();
        booksDoc.forEach(bookList::add);

        return getFormattedResultBooks(bookList);
    }

    private static void addFilter(String field, String value, List<Bson> filters) {
        if (!value.isEmpty()) {
            filters.add(Filters.regex(field, Pattern.compile(value, Pattern.CASE_INSENSITIVE)));
        }
    }

    private static String getFormattedResultBooks(List<Document> bookList) {
        StringBuilder output = new StringBuilder();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        int lines_counter = 0;

        for (Document doc : bookList) {
            Date date = doc.getDate("releaseDate");
            String dateString = formatter.format(date);

            output.append("\nID: ").append(doc.get("_id")).append("\n")
                    .append("Tytuł: ").append(doc.get("title")).append("\n")
                    .append("Wydawnictwo: ").append(doc.get("publishingHouse")).append("\n")
                    .append("Data wydania: ").append(dateString).append("\n");
            if (doc.get("series") != null) {
                output.append("Seria: ").append(doc.get("series")).append("\n")
                        .append("Tom serii: ").append(doc.get("tomeOfSeries")).append("\n");
            }
            output.append("ISBN: ").append(doc.get("codeISBN")).append("\n")
                    .append("Ilość stron: ").append(doc.get("numberOfPages"));
            List<String> genres = (List<String>) doc.get("genre");
            if (genres.size() == 1) {
                output.append("\nGatunek: ");
            } else if (genres.size() == 0) {
                output.append("\nGatunek: brak");
            } else {
                output.append("\nGatunki: ");
            }
            int i = 0;
            for (String genre : genres) {
                output.append(genre);
                if (genres.size() > 1) {
                    i++;
                    if (genres.size() > i) {
                        output.append(", ");
                    }
                }
            }

            List<String> authors = (List<String>) doc.get("authors");
            if (authors.size() == 1) {
                output.append("\nAutor: ");
            } else if (authors.size() == 0) {
                output.append("\nAutor: brak");
            } else {
                output.append("\nAutorzy: ");
            }
            int j = 0;
            for (String author : authors) {
                output.append(author);
                if (authors.size() > 1) {
                    j++;
                    if (authors.size() > j) {
                        output.append(", ");
                    }
                }
            }

            output.append("\n");
            ++lines_counter;
        }

        if (lines_counter == 0) {
            output.append("Nie znaleziono żadnych książek.\n");
        }
        return output.toString();
    }

    public static String printAllReaders() {

        FindIterable<Document> iterDoc = readers.find();
        Iterator<Document> it = iterDoc.iterator();
        StringBuilder output = new StringBuilder();
        int lines_counter = 0;

        it.next();
        while (it.hasNext()) {
            Document doc = it.next();

            output.append("\nID: ").append(doc.get("_id")).append("\nNazwa użytkownika: ").append(doc.get("userName")).append("\n").append("Imię: ").append(doc.get("personalInfo", Document.class).get("name")).append("\n");
            if (doc.get("personalInfo", Document.class).get("secondName") != null) {
                output.append("Drugie imię: ").append(doc.get("personalInfo", Document.class).get("secondName"));
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = doc.get("personalInfo", Document.class).getDate("dateOfBirth");
            String dateString = formatter.format(date);

            output.append("\n" + "Nazwisko: ").append(doc.get("personalInfo", Document.class).get("lastName")).append("\nData urodzenia: ").append(dateString).append("\nPesel: ").append(doc.get("personalInfo", Document.class).get("peselNumber")).append("\nAdres: ").append(doc.get("address", Document.class).get("street")).append(" ").append(doc.get("address", Document.class).get("houseNumber"));

            if (doc.get("address", Document.class).get("apartmentNumber") != null) {
                output.append("/").append(doc.get("address", Document.class).get("apartmentNumber"));
            }

            output.append(" ").append(doc.get("address", Document.class).get("city")).append(" ").append(doc.get("address", Document.class).get("province")).append(" ").append(doc.get("address", Document.class).get("country")).append("\n").append("E-mail: ").append(doc.get("contacts", Document.class).get("email")).append("\n").append("Numer telefonu: ").append(doc.get("contacts", Document.class).get("phoneNumber"));

            List<String> borrow = (List<String>) doc.get("borrows");
            if(borrow != null) {
                int i = 0;
                if (borrow.size() == 1) {
                    output.append("\nID wypożyczenia uzytkownika: ");
                } else if (borrow.size() == 0) {
                    output.append("\nID wypożyczenia użytkownika: brak");
                } else {
                    output.append("\nID wypożyczeń użytkownika: ");
                }
                for (String b : borrow) {
                    output.append(b);
                    if (borrow.size() > 1) {
                        i++;
                        if (borrow.size() > i) {
                            output.append(", ");
                        }
                    }
                }
            }


            output.append("\n");
            lines_counter++;
        }

        if (lines_counter == 0) {
            output.setLength(0);
            output.append("Brak czytelników.");
            output.append("\n");
        }

        return output.toString();
    }

    public static String printAllAuthors() {

        FindIterable<Document> iterDoc = authors.find();
        Iterator<Document> it = iterDoc.iterator();
        StringBuilder output = new StringBuilder();
        int lines_counter = 0;

        it.next();
        while (it.hasNext()) {
            Document doc = it.next();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = doc.getDate("dateOfBirth");
            String dateString = formatter.format(date);

            output.append("\nImię i nazwisko autora: ").append(doc.get("name")).append("\n").append("Data urodzenia: ").append(dateString);

            List<String> book = (List<String>) doc.get("books");
            if(book!=null) {
                if (book.size() == 1) {
                    output.append("\nKsiążka: ");
                } else if (book.size() == 0) {
                    output.append("\nKsiążka: brak");
                } else {
                    output.append("\nKsiążki: ");
                }
                int i = 0;
                for (String b : book) {
                    output.append(b);
                    if (book.size() > 1) {
                        i++;
                        if (book.size() > i) {
                            output.append(", ");
                        }
                    }
                }
            }
            output.append("\n");
            lines_counter++;
        }

        if (lines_counter == 0) {
            output.setLength(0);
            output.append("Brak autorów.");
            output.append("\n");
        }

        return output.toString();
    }

    private static String getFormattedResultBorrows(List<Document> borrowsList) {
        StringBuilder output = new StringBuilder();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        int lines_counter = 0;

        for (Document doc : borrowsList) {
            java.util.Date date = doc.getDate("dateOfBorrow");
            String dateOfBorrow = formatter.format(date);
            output.append("\nData wypożyczenia książki: ").append(dateOfBorrow).append("\nData oddania książki: ");
            java.util.Date date1 = doc.getDate("endOfBorrow");

            if (date1 == null) {
                output.append(" książka jest nadal wypożyczona");

            } else{
                String dateOfEnd = formatter.format(date1);
                    output.append(dateOfEnd);
            }
                output.append("\nWypożyczona książka:\n     ID: ").append(doc.get("book", Document.class).get("bookId")).append("\n     Tytuł: ").append(doc.get("book", Document.class).get("bookTitle"));

                List<String> books = (List<String>) doc.get("book", Document.class).get("bookAuthors");

                if (books.size() == 1) {
                    output.append("\n     Autor: ");
                } else if (books.size() == 0) {
                    output.append("\n     Autor: brak");
                } else {
                    output.append("\n     Autorzy: ");
                }
                int i = 0;
                for (String book : books) {
                    output.append(book);
                    if (books.size() > 1) {
                        i++;
                        if (books.size() > i) {
                            output.append(", ");
                        }
                    }
                }

                if (!doc.get("delay", Document.class).get("daysOfDelay").equals(0)) {
                    output.append("\nOpóźnienie:  \n     Ilość dni: ").append(doc.get("delay", Document.class).get("daysOfDelay")).append("\n     Kara: ").append(doc.get("delay", Document.class).get("fee"));
                }

                output.append("\n");
                lines_counter++;
            }


        if (lines_counter == 0) {
            output.append("Nie znaleziono wypożyczeń.\n");
        }
        return output.toString();
    }
    public static String printAllBorrows() {

        FindIterable<Document> iterDoc = borrows.find();
        Iterator<Document> it = iterDoc.iterator();
        StringBuilder output = new StringBuilder();
        int lines_counter = 0;

        it.next();
        while (it.hasNext()) {
            Document doc = it.next();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = doc.getDate("dateOfBorrow");
            String dateOfBorrow = formatter.format(date);

            output.append("\nID: ").append(doc.get("_id")).append("\nData wypożyczenia książki: ").append(dateOfBorrow).append("\nData oddania książki: ");
            java.util.Date date1 = doc.getDate("endOfBorrow");
            if (date1 == null) {
                output.append(" książka jest nadal wypożyczona");
            } else {
                String endOfBorrow = formatter.format(date);
                output.append(endOfBorrow);
            }
            output.append("\nWypożyczona książka:\n     ID: ").append(doc.get("book", Document.class).get("bookId")).append("\n     Tytuł: ").append(doc.get("book", Document.class).get("bookTitle"));

            List<String> books = (List<String>) doc.get("book", Document.class).get("bookAuthors");
            if(books!=null) {

                if (books.size() == 1) {
                    output.append("\n     Autor: ");
                } else if (books.size() == 0) {
                    output.append("\n     Autor: brak");
                } else {
                    output.append("\n     Autorzy: ");
                }
                int i = 0;
                for (String book : books) {
                    output.append(book);
                    if (books.size() > 1) {
                        i++;
                        if (books.size() > i) {
                            output.append(", ");
                        }
                    }
                }
            }
            output.append("\nCzytelnik: \n     ID: ").append(doc.get("reader", Document.class).get("readerId")).append("\n     Nazwa użytkownika: ").append(doc.get("reader", Document.class).get("readerUserName"));

            if (!doc.get("delay", Document.class).get("daysOfDelay").equals(0)) {
                output.append("\nOpóźnienie:  \n     Ilość dni: ").append(doc.get("delay", Document.class).get("daysOfDelay")).append("\n     Kara: ").append(doc.get("delay", Document.class).get("fee"));
            }

            output.append("\n");
            lines_counter++;
        }

        if (lines_counter == 0) {
            output.setLength(0);
            output.append("Brak wypożyczeń.");
            output.append("\n");
        }

        return output.toString();
    }

    public static String printUserBorrows() {

        Document filter = new Document("reader.readerUserName", GlobalVariables.userName).append("endOfBorrow", null);
        FindIterable<Document> iterDoc = borrows.find(filter);

        List<Document> borrowsList = new ArrayList<>();
        iterDoc.forEach(borrowsList::add);

        return getFormattedResultBorrows(borrowsList);
    }

    public static String printAllUserBorrows() {

        FindIterable<Document> iterDoc = borrows.find(eq("reader.readerUserName", GlobalVariables.userName));

        List<Document> borrowsList = new ArrayList<>();
        iterDoc.forEach(borrowsList::add);

        return getFormattedResultBorrows(borrowsList);
    }

    public static boolean checkBookExists(String id) {

        Document filter = new Document("_id", id);
        FindIterable<Document> result = books.find(filter);
        return result.first() != null;
    }


    public static String getTitleForBorrow(String booksId) {

        Document bookQuery = new Document("_id", booksId);
        Document bookDoc = books.find(bookQuery).first();

        assert bookDoc != null;
        return bookDoc.getString("title");
    }

    public static List<String> getAuthorsForBorrow(String booksId) {

        Document bookQuery = new Document("_id", booksId);
        Document bookDoc = books.find(bookQuery).first();

        assert bookDoc != null;
        return (List<String>) bookDoc.get("authors");
    }

    public static String getReaderId(String userName) {

        Document bookQuery = new Document("userName", userName);
        Document bookDoc = readers.find(bookQuery).first();

        assert bookDoc != null;
        return bookDoc.get("_id").toString();
    }


    public static Boolean checkIfBookIsBorrowed(String bookId) {

        Document query = new Document("book.bookId", bookId).append("delay.isReturned", false);
        Document borrowedBook = borrows.find(query).first();

        return borrowedBook != null;
    }

    public static String borrowBook(String booksId) {


        String borrowsId = getLastIdFromBorrows();
        String userName = GlobalVariables.userName;
        Date date = new Date();


        String title = getTitleForBorrow(booksId);
        List<String> authors = getAuthorsForBorrow(booksId);
        String readerId = getReaderId(userName);

        Document book = new Document("bookId", booksId)
                .append("bookTitle", title)
                .append("bookAuthors", authors);
        Document reader = new Document("readerId", readerId)
                .append("readerUserName", userName);
        Document delay = new Document("daysOfDelay", 0)
                .append("isReturned", false)
                .append("fee", 0);

        Document newBorrow = new Document();
        newBorrow.append("_id", borrowsId);
        newBorrow.append("dateOfBorrow", date);
        newBorrow.append("endOfBorrow", null);
        newBorrow.append("book", book);
        newBorrow.append("reader", reader);
        newBorrow.append("delay", delay);
        borrows.insertOne(newBorrow);


        readers.updateOne(
                eq("_id", readerId),
                new Document("$addToSet", new Document("borrows", borrowsId))
        );

        return "Książka " + title + " o ID: " + booksId + " została wypożyczona.\n";
    }

    public static String getBorrowId(String bookId, String userName) {

        Document filter = new Document("book.bookId", bookId).append("reader.readerUserName", userName).append("delay.isReturned", false);
        FindIterable<Document> result = borrows.find(filter);

        if (result.first() != null) {
            return Objects.requireNonNull(result.first()).getString("_id");
        } else {
            return null;
        }
    }

    public static String returnBook(String booksId) {

        String userName = GlobalVariables.userName;
        Date date = new Date();
        String borrowId = getBorrowId(booksId, userName);
        Bson filter = eq("_id", borrowId);

        Document borrowDoc = borrows.find(eq("_id", borrowId)).first();
        assert borrowDoc != null;
        Date borrowDate = borrowDoc.getDate("dateOfBorrow");
        long loanTimestamp = borrowDate.getTime();
        long currentTimestamp = new Date().getTime();
        long diff = currentTimestamp - loanTimestamp;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        if (diffDays > 30) {
            long delayDays = diffDays - 30;
            double penalty;
            penalty = (delayDays / 7.0) * 5;

            Bson update = combine(set("endOfBorrow", date), set("delay.isReturned", true), set("delay.daysOfDelay", delayDays), set("delay.fee", penalty));
            UpdateResult result = borrows.updateOne(filter, update);
            if (result.getModifiedCount() > 0) {
                if(delayDays==1){
                    return "Książka o ID: " + booksId + " została zwrócona. Z opóźnieniem " + delayDays + " dnia, kara wynosi: " + penalty + "\n";
                }
                return "Książka o ID: " + booksId + " została zwrócona. Z opóźnieniem " + delayDays + " dni, kara wynosi: " + penalty + "\n";
            } else {
                return "Zwrócenie książki nie jest możliwe.\n";
            }
        } else {

            Bson update = combine(set("endOfBorrow", date), set("delay.isReturned", true));
            UpdateResult result = borrows.updateOne(filter, update);
            if (result.getModifiedCount() > 0) {
                return "Książka o ID: " + booksId + " została zwrócona.\n";
            } else {
                return "Zwrócenie książki nie jest możliwe.\n";
            }
        }

    }

    public static Boolean addBookToAuthor(String authorName, String bookTitle) {

        Document author = authors.find(new Document("name", authorName)).first();
        if (author == null) {
            return false;
        }

        authors.updateOne(
                new Document("_id", author.get("_id")),
                new Document("$push", new Document("books", bookTitle))
        );

        return true;
    }

    public static boolean addAuthor(String id, String fullName, Date dateOfBirth, ArrayList<String> books){
        try {
            Document author = new Document("_id", id);
            author.append("name", fullName);
            author.append("dateOfBirth", dateOfBirth);
            author.append("books", books);

            authors.insertOne(author);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkIfAuthorExists(String name) {

        Bson filter = Filters.regex("name", Pattern.compile(name, Pattern.CASE_INSENSITIVE));
        FindIterable<Document> authorsDoc = authors.find(filter);
        return authorsDoc.iterator().hasNext();
    }

    private static Document getLastDocument(MongoCollection<Document> collection) {
        long count = collection.countDocuments();
        return collection.find().skip((int) (count - 1)).first();
    }

    public static String getLastIdFromBooks() {

        Document lastDoc = getLastDocument(books);

        if (lastDoc == null) return "1"; //if collection is empty
        String last_id = lastDoc.getString("_id");
        int newNumber = Integer.parseInt(last_id.replaceAll("[^0-9]", "")) + 1;
        return String.valueOf(newNumber);
    }

    public static String getLastIdFromAuthors() {
        Document lastDoc = getLastDocument(authors);

        if (lastDoc == null) return "1a"; //if collection is empty
        String last_id = lastDoc.getString("_id");
        int newNumber = Integer.parseInt(last_id.replaceAll("[^0-9]", "")) + 1;
        return newNumber + "a";
    }

    public static String getLastIdFromBorrows() {
        Document lastDoc = getLastDocument(borrows);

        if (lastDoc == null) return "1b"; //if collection is empty
        String last_id = lastDoc.getString("_id");
        int newNumber = Integer.parseInt(last_id.replaceAll("[^0-9]", "")) + 1;
        return newNumber + "b";
    }

    public static String getLastIdFromReaders() {
        Document lastDoc = getLastDocument(readers);

        if (lastDoc == null) return "1r"; //if collection is empty
        String last_id = lastDoc.getString("_id");
        int newNumber = Integer.parseInt(last_id.replaceAll("[^0-9]", "")) + 1;
        return newNumber + "r";
    }

}
